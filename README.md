# Astropath

Astropath is a discord bot developed by [Brotherhood of the Void](https://inara.cz/squadron/7401/) to aid coordinating our activities in the [Elite Dangerous](https://www.elitedangerous.com) universe.

## Current Status

### Release
![Release Badge](https://img.shields.io/static/v1?label=go.mod&message=v0.2.0&color=blue)

[![pipeline status](https://gitlab.com/kintar1900/ed-astropath/badges/master/pipeline.svg)](https://gitlab.com/kintar1900/ed-astropath/-/commits/master)
[![coverage report](https://gitlab.com/kintar1900/ed-astropath/badges/master/coverage.svg)](https://gitlab.com/kintar1900/ed-astropath/-/commits/master)

### Development
![Dev Badge](https://img.shields.io/static/v1?label=go.mod&message=v0.1.0-rc.29-g4ed012c&color=blue)

[![pipeline status](https://gitlab.com/kintar1900/ed-astropath/badges/develop/pipeline.svg)](https://gitlab.com/kintar1900/ed-astropath/-/commits/develop)
[![coverage report](https://gitlab.com/kintar1900/ed-astropath/badges/develop/coverage.svg)](https://gitlab.com/kintar1900/ed-astropath/-/commits/develop)


# Documentation

Please see our [Wiki](https://gitlab.com/kintar1900/ed-astropath/-/wikis/home).