package put

import (
	"astropath/api"
	"astropath/datastore"
	"github.com/stretchr/testify/assert"
	"gitlab.com/kintar1900/elite-journalfile/event"
	"testing"
)

type mockRepo struct {
	entries                                   map[string]map[string]datastore.Entry
	putCount, getCount, deleteCount, allCount int
}

func newMockRepo() *mockRepo {
	return &mockRepo{
		entries: make(map[string]map[string]datastore.Entry),
	}
}

func getSubMap(dataType string, entries map[string]map[string]datastore.Entry) map[string]datastore.Entry {
	subMap, ok := entries[dataType]
	if !ok {
		subMap = make(map[string]datastore.Entry)
		entries[dataType] = subMap
	}
	return subMap
}

func (m *mockRepo) Put(entry datastore.Entry) error {
	m.putCount++

	subMap := getSubMap(entry.DataType(), m.entries)

	existing, ok := subMap[entry.Id()]
	if ok {
		if entry.Version() != existing.Version() {
			return datastore.NewConcurrentUpdateError(entry, nil)
		}
	}

	entry.IncrementVersion()
	subMap[entry.Id()] = entry

	return nil
}

func (m *mockRepo) Get(id string, dataType string) (datastore.Entry, error) {
	m.getCount++

	subMap := getSubMap(dataType, m.entries)

	existing, ok := subMap[id]
	if ok {
		return existing, nil
	}
	return nil, nil
}

func (m *mockRepo) All(dataType string) ([]datastore.Entry, error) {
	m.allCount++
	subMap := getSubMap(dataType, m.entries)
	results := make([]datastore.Entry, len(subMap))
	i := 0

	for _, e := range subMap {
		results[i] = e
		i++
	}

	return results, nil
}

func (m *mockRepo) Delete(entry datastore.Entry) error {
	m.deleteCount++
	subMap := getSubMap(entry.DataType(), m.entries)
	delete(subMap, entry.Id())
	return nil
}

func (m mockRepo) Verify(t *testing.T, expectedGets, expectedPuts, expectedDeletes, expectedAll int) {
	assert.Equal(t, expectedAll, m.allCount, "wrong number of All() invocations")
	assert.Equal(t, expectedGets, m.getCount, "wrong number of Get() invocations")
	assert.Equal(t, expectedPuts, m.putCount, "wrong number of Put() invocations")
	assert.Equal(t, expectedDeletes, m.deleteCount, "wrong number of Delete() invocations")
}

func authData() api.AuthData {
	return api.AuthData{CommanderName: "testboi"}
}

func TestFsdJumpNoCommander(t *testing.T) {
	repo := newMockRepo()

	jump := api.PutData{
		AuthData: authData(),
		EventMap: nil,
		Event: &event.FSDJump{
			StarSystem: "",
			StarPos:    make([]float64, 3, 3),
		},
	}

	err := StoreEvent(jump.CommanderName, jump.Event, repo)
	assert.Nil(t, err, "error returned: ", err)

	repo.Verify(t, 1, 1, 0, 0)
}

func TestFsdJumpExistingCommander(t *testing.T) {
	repo := newMockRepo()

	origEntry, _ := datastore.NewEntry(datastore.EntryCommanderLocation, "testboi")
	origEntry.IncrementVersion()

	cmdr := origEntry.(*datastore.CommanderLocation)
	cmdr.CurrentSystem = datastore.System{
		Name: "Nowhere",
	}

	subMap := getSubMap(cmdr.DataType(), repo.entries)
	subMap[cmdr.Id()] = cmdr

	jump := api.PutData{
		AuthData: authData(),
		EventMap: nil,
		Event: &event.FSDJump{
			StarSystem: "Somewhere",
			StarPos:    make([]float64, 3, 3),
		},
	}

	err := StoreEvent(jump.CommanderName, jump.Event, repo)
	assert.Nil(t, err, "error returned: ", err)

	repo.Verify(t, 1, 1, 0, 0)

	entry := repo.entries[cmdr.DataType()][cmdr.Id()].(*datastore.CommanderLocation)

	assert.Equal(t, 2, entry.Version())
	assert.Equal(t, "Somewhere", entry.CurrentSystem.Name)
}
