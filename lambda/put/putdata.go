// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package put

import (
	"astropath/datastore"
	"errors"
	"fmt"
	"gitlab.com/kintar1900/elite-journalfile/event"
	"time"
)

func StoreEvent(cmdrName string, evt event.Event, repo datastore.Repository) error {
	switch actual := evt.(type) {
	case *event.FSDJump:
		return updateCommander(*actual, cmdrName, repo)
	case *event.CarrierJumpRequest:
		return processJumpRequest(*actual, repo)
	case *event.CarrierJump:
		return processJump(*actual, repo)
	case *event.CarrierJumpCancelled:
		return cancelJump(*actual, repo)
	case *event.CarrierStats:
		return updateCarrier(*actual, repo)
	default:
		return errors.New(fmt.Sprintf("unsupported event type '%s'", string(evt.EventName())))
	}
}

func updateCarrier(stats event.CarrierStats, repo datastore.Repository) error {
	carrierId := string(stats.CarrierID)
	existing, err := repo.Get(carrierId, datastore.EntryCarrierName)
	if err != nil {
		return err
	}

	if existing == nil {
		existing, err = datastore.NewEntry(datastore.EntryCarrierName, carrierId)
		if err != nil {
			return err
		}
	}

	actual := existing.(*datastore.CarrierName)
	actual.CarrierName = stats.Name
	actual.Callsign = stats.Callsign

	return repo.Put(actual)
}

func processJump(jump event.CarrierJump, repo datastore.Repository) error {
	carrierId := string(jump.MarketID)
	err := deleteExistingJumpPlan(carrierId, repo)
	if err != nil {
		return err
	}

	carrier, err := repo.Get(carrierId, datastore.EntryCarrierLocation)
	if err != nil {
		return err
	}

	if carrier == nil {
		carrier, err = datastore.NewEntry(datastore.EntryCarrierLocation, carrierId)
		if err != nil {
			return err
		}
	}

	actual, ok := carrier.(*datastore.CarrierLocation)
	if !ok {
		return errors.New("could not create CarrierLocation")
	}

	actual.Name = jump.StationFaction.Name
	actual.Callsign = jump.StationName
	actual.CurrentSystem = datastore.System{
		Name: jump.StarSystem,
		X:    jump.StarPos[0],
		Y:    jump.StarPos[1],
		Z:    jump.StarPos[2],
	}

	return repo.Put(actual)
}

func cancelJump(event event.CarrierJumpCancelled, repo datastore.Repository) error {
	return deleteExistingJumpPlan(string(event.CarrierID), repo)
}

func deleteExistingJumpPlan(carrierId string, repo datastore.Repository) error {
	jumpPlan, err := repo.Get(carrierId, datastore.EntryJumpPlan)
	if err != nil {
		return err
	}

	if jumpPlan != nil {
		return repo.Delete(jumpPlan)
	}

	return nil
}

func processJumpRequest(request event.CarrierJumpRequest, repo datastore.Repository) error {
	carrierId := string(request.CarrierID)
	jumpPlan, err := datastore.NewEntry(datastore.EntryJumpPlan, carrierId)
	if err != nil {
		return err
	}

	carrier, err := repo.Get(carrierId, datastore.EntryCarrierLocation)
	if err != nil {
		return err
	}

	carrierName, err := repo.Get(carrierId, datastore.EntryCarrierName)
	if err != nil {
		return err
	}

	actual := jumpPlan.(*datastore.JumpPlan)

	actual.FromSystem = datastore.System{Name: "Unknown"}

	if carrier != nil {
		realCarrier := carrier.(*datastore.CarrierLocation)
		actual.Callsign = realCarrier.Callsign
		actual.CarrierName = realCarrier.Name
		actual.FromSystem = realCarrier.CurrentSystem
	} else if carrierName != nil {
		cn := carrierName.(*datastore.CarrierName)
		actual.Callsign = cn.Callsign
		actual.CarrierName = cn.CarrierName
	} else {
		actual.Callsign = "UNKNOWN"
		actual.CarrierName = "UNKNOWN"
	}

	actual.ToSystem = datastore.System{
		Name: request.SystemName,
	}

	return repo.Put(actual)
}

func updateCommander(jump event.FSDJump, commanderName string, repo datastore.Repository) error {
	entry, err := repo.Get(commanderName, datastore.EntryCommanderLocation)
	if err != nil {
		return err
	}

	if entry == nil {
		entry, err = datastore.NewEntry(datastore.EntryCommanderLocation, commanderName)
		if err != nil {
			return err
		}
	}

	cmdr, ok := entry.(*datastore.CommanderLocation)
	if !ok {
		return errors.New("unexpected error converting entry to CommanderLocation")
	}

	cmdr.CurrentSystem = datastore.System{
		Name: jump.StarSystem,
		X:    jump.StarPos[0],
		Y:    jump.StarPos[1],
		Z:    jump.StarPos[2],
	}

	cmdr.LastBeacon = time.Now().UTC()

	err = repo.Put(cmdr)
	return err
}
