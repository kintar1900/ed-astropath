// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"astropath/api"
	"astropath/datastore"
	"astropath/lambda/put"
	"errors"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/kintar1900/elite-journalfile/event"
	"log"
)

type Router interface {
	Route(request events.APIGatewayV2HTTPRequest) events.APIGatewayV2HTTPResponse
}

type defaultRouter struct {
	repository datastore.Repository
	validator  api.Validator
}

func (r defaultRouter) Route(request events.APIGatewayV2HTTPRequest) events.APIGatewayV2HTTPResponse {
	switch request.RequestContext.HTTP.Method {
	case "GET":
		return r.routeGetRequest(request.Body)
	case "PUT":
		return r.routePutRequest(request.Body)
	default:
		return events.APIGatewayV2HTTPResponse{
			StatusCode: 405,
			Body:       fmt.Sprintf("bad request: method '%s' is not supported", request.RequestContext.HTTP.Method),
		}
	}
}

func (r defaultRouter) routeGetRequest(requestBody string) events.APIGatewayV2HTTPResponse {
	panic("not implemented")
}

func (r defaultRouter) routePutRequest(requestBody string) events.APIGatewayV2HTTPResponse {
	authData, ok := api.ParseAuthData(requestBody)

	if !ok {
		log.Println("could not parse authdata from body: '", requestBody, "'")
		return events.APIGatewayV2HTTPResponse{
			Body:       fmt.Sprintf("Bad Request"),
			StatusCode: 400,
		}
	}

	if !r.validator.Validate(authData) {
		log.Println("validiator says: NO AUTH FOR YOU!")
		return events.APIGatewayV2HTTPResponse{
			Body:       fmt.Sprintf("Unauthorized"),
			StatusCode: 401,
		}
	}

	putData, err := api.ParsePutDataRequest(requestBody)
	if err != nil {
		if !errors.As(err, event.ErrUnrecognizedEvent) {
			return events.APIGatewayV2HTTPResponse{
				Body:       fmt.Sprintf("bad message body: %s", err.Error()),
				StatusCode: 400,
			}
		} else {
			return events.APIGatewayV2HTTPResponse{
				StatusCode: 202,
				Body:       "event type not interesting, thanks",
			}
		}
	}

	err = put.StoreEvent(putData.CommanderName, putData.Event, r.repository)
	if err != nil {
		return events.APIGatewayV2HTTPResponse{
			Body:       err.Error(),
			StatusCode: 500,
		}
	}

	return events.APIGatewayV2HTTPResponse{
		StatusCode: 200,
		Body:       "OK",
	}
}
