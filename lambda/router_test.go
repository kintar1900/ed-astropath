// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"astropath/api"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"log"
	"testing"
)

func Test_DisallowedMethods(t *testing.T) {
	t.Parallel()
	router := defaultRouter{}
	for _, method := range []string{
		"HEAD", "POST", "DELETE", "CONNECT", "OPTIONS", "TRACE",
	} {
		t.Run(method, func(t *testing.T) {
			request := events.APIGatewayV2HTTPRequest{
				RequestContext: events.APIGatewayV2HTTPRequestContext {
					HTTP: events.APIGatewayV2HTTPRequestContextHTTPDescription{
						Method: method,
					},
				},
			}
			response := router.Route(request)
			assert.Equal(t, 405, response.StatusCode)
		})
	}
}

func Test_AllowedMethods(t *testing.T) {
	t.Parallel()
	router := defaultRouter{validator: mockValidator{}}
	for _, method := range []string{
		"PUT",
	} {
		t.Run(method, func(t *testing.T) {
			request := events.APIGatewayV2HTTPRequest{
				RequestContext: events.APIGatewayV2HTTPRequestContext {
					HTTP: events.APIGatewayV2HTTPRequestContextHTTPDescription{
						Method: method,
					},
				},
				Body: `{ "Commander": "Kintar", "Token": "none", "Event": {} }`,
			}
			response := router.Route(request)
			assert.NotEqual(t, 405, response.StatusCode)
		})
	}
}

type mockValidator struct{}

func (m mockValidator) Validate(req api.AuthData) bool {
	return req.Token != ""
}

func Test_PutResponseCodesUnauthorized(t *testing.T) {
	t.Parallel()
	router := defaultRouter{
		validator: mockValidator{},
	}

	for i, data := range []struct {
		jsonData      string
		expectedCodee int
	}{
		{
			"{}",
			401,
		},
		{
			`{ "Commander": "Foo", "ApiKey": "", "Event": { "event": "blerg" } }`,
			401,
		},
		{
			`{ "Commander": "Foo", "Event" : { "event": "FSDJump" } }`,
			401,
		},
		{
			`{ """`,
			400, // Bad data
		},
	} {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			response := router.routePutRequest(data.jsonData)
			if !assert.Equal(t, data.expectedCodee, response.StatusCode) {
				log.Println(response.Body)
			}
		})
	}
}
