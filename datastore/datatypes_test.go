// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package datastore

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewEntry(t *testing.T) {
	for _, eType := range []string{EntryCarrierLocation, EntryCommanderLocation, EntryJumpPlan} {
		t.Run("new_"+eType, func(t *testing.T) {
			e, err := NewEntry(eType, "test")
			assert.Nil(t, err, "error creating new instance: ", err)
			assert.NotNil(t, e, "new instance was nil")
		})
	}
}
