// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package datastore

import (
	"fmt"
)

type ConcurrentUpdateError struct {
	entry   Entry
	wrapped error
}

func NewConcurrentUpdateError(entry Entry, err error) error {
	return &ConcurrentUpdateError{entry, err}
}

func (e *ConcurrentUpdateError) Error() string {
	return fmt.Sprintf("concurrent update: '%s'@'%s'.'%d'", e.entry.DataType(), e.entry.Id(), e.entry.Version())
}

func (e *ConcurrentUpdateError) Unwrap() error {
	return e.wrapped
}

// Putter places new entries in the data store.
//
// All methods return a non-nil error if there was a failure to write to the store.
type Putter interface {
	// Writes the given Entry into the data store, returning ConcurrentUpdateError if the entity exists and is at
	// a higher version.
	Put(entry Entry) error
}

// Getter retrieves entries from the data store
type Getter interface {
	// Get retrieves a single Entry with the given name and data type from the store.
	//
	// Returns nil, nil if the entry does not exist, or nil and an error if there was an error in the underlying store.
	Get(id string, dataType string) (Entry, error)

	// All retrieves every entry of a given type from the data store.
	//
	// Returns nil, nil if there are no entries of the given type, or nil and an error if there was an error in the
	// underlying store.
	All(dataType string) ([]Entry, error)
}

// Deleter erases entries from the data store
type Deleter interface {
	// Delete will remove the specified entry from the store if it exists.  Returns nil if the entry does not exist,
	// a ConcurrentUpdateError if the entry exists but is at a different version, or an underlying data store error.
	Delete(entry Entry) error
}

// Repository wraps the Putter, Getter, and Deleter interfaces into a single interface
type Repository interface {
	Putter
	Getter
	Deleter
}
