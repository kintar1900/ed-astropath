// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// +build integration

package dynamodb

import (
	"astropath/datastore"
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/stretchr/testify/assert"
	"log"
	"os"
	"testing"
)

func makeSession() *session.Session {
	if _, ok := os.LookupEnv("AWS_REGION"); !ok {
		_ = os.Setenv("AWS_REGION", "us-east-2")
	}
	return session.Must(session.NewSession())
}

var testData = []datastore.Entry{createTestCarrier(), createTestCommander(), createJumpPlan(), createCarrerName()}

func createCarrerName() datastore.Entry {
	cn, _ := datastore.NewEntry(datastore.EntryCarrierName, "10020")
	carrierName := cn.(*datastore.CarrierName)
	carrierName.CarrierName = "Flighty Boi"
	carrierName.Callsign = "TC-2001"
	return carrierName
}

func TestMain(t *testing.M) {
	log.Println("beginning tests:")
	os.Exit(t.Run())
}

func deleteEntry(entry datastore.Entry) {
	key, err := dynamodbattribute.MarshalMap(entry.Key())
	if err != nil {
		log.Fatal("could not create key for deleting test data: ", err)
	}

	deleteInput := &dynamodb.DeleteItemInput{
		TableName: GetTableName(),
		Key:       key,
	}

	_, err = dynamodb.New(makeSession()).DeleteItem(deleteInput)
	if err != nil {
		panic(fmt.Sprint("failed to delete test data: ", err))
	}
}

func TestItemPutsAndLocking(t *testing.T) {
	repo := NewRepositoryFromSession(makeSession())

	for _, entry := range []datastore.Entry{createTestCarrier(), createTestCommander()} {
		deleteEntry(entry)
		err := repo.Put(entry)
		assert.Nil(t, err, "could not put entry: ", err)
		assert.Equal(t, 1, entry.Version(), "version was not incremented")

		entry2, err := datastore.NewEntry(entry.DataType(), entry.Id())

		err = repo.Put(entry2)
		if entry2.DataType() == datastore.EntryJumpPlan {
			assert.Nil(t, err, "overwriting existing jump plan failed")
		} else {
			if assert.NotNil(t, err, "overwriting existing entry with new was allowed") {
				_, ok := err.(*datastore.ConcurrentUpdateError)
				assert.True(t, ok, "concurrent modification returned wrong error type: %T", err)
			}

			err = repo.Put(entry)

			assert.Nil(t, err, "error returned updating entry: ", entry, err)
			assert.Equal(t, 2, entry.Version(), "wrong version number")
		}
	}
}

func TestGetAll(t *testing.T) {
	repo := NewRepositoryFromSession(makeSession())

	for _, entryType := range []string{datastore.EntryJumpPlan, datastore.EntryCommanderLocation, datastore.EntryCarrierLocation} {
		for _, name := range []string{"OneGuy", "TwoGuy", "ThreeGuy"} {
			entry, err := datastore.NewEntry(entryType, name)
			deleteEntry(entry)
			assert.Nil(t, err, "could not create entry")
			err = repo.Put(entry)
			assert.Nil(t, err, "could not put entry")
		}
		entries, err := repo.All(entryType)
		assert.Nil(t, err, "failed to fetch all: ", err)
		assert.Greater(t, len(entries), 2, "wrong number of entries")
	}
}

func TestGetItem(t *testing.T) {
	repo := NewRepositoryFromSession(makeSession())

	for _, entry := range testData {
		deleteEntry(entry)
		t.Run(entry.Id(), func(t *testing.T) {

			err := repo.Put(entry)
			assert.Nil(t, err, "could not write entry to ddb", err)

			r, err := repo.Get(entry.Id(), entry.DataType())
			assert.Nil(t, err, "failed to read entry: ", err)

			assert.Equal(t, entry, r, "loaded entry is not a match")
		})
	}
}

func TestGetNonexistentItem(t *testing.T) {
	repo := NewRepositoryFromSession(makeSession())

	entry, err := repo.Get("no such entry exists, I guarantee", datastore.EntryJumpPlan)
	assert.Nil(t, err)
	assert.Nil(t, entry)
}

func TestRepo_Delete(t *testing.T) {
	repo := NewRepositoryFromSession(makeSession())

	for _, entry := range []datastore.Entry{createTestCommander(), createTestCarrier(), createJumpPlan()} {
		_ = repo.Put(entry)
		err := repo.Delete(entry)
		assert.Nil(t, err, "error deleting entry: ", err)
	}
}

func Test_NoPutNil(t *testing.T) {
	repo := NewRepositoryFromSession(makeSession())

	err := repo.Put(nil)
	assert.NotNil(t, err, "allowed to put a nil entry")
}

func newSystem(name string) datastore.System {
	return datastore.System{
		Name: name,
		X:    0,
		Y:    1,
		Z:    2,
	}
}

func createTestCarrier() *datastore.CarrierLocation {
	entry, _ := datastore.NewEntry(datastore.EntryCarrierLocation, "SS Testy Boi")
	carrier, _ := entry.(*datastore.CarrierLocation)

	carrier.CurrentSystem = newSystem("nowhere")
	return carrier
}

func createTestCommander() *datastore.CommanderLocation {
	entry, _ := datastore.NewEntry(datastore.EntryCommanderLocation, "Cmdr Rick Hammer")
	commander, _ := entry.(*datastore.CommanderLocation)

	commander.CurrentSystem = newSystem("nowhere")
	return commander

}

func createJumpPlan() *datastore.JumpPlan {
	entry, _ := datastore.NewEntry(datastore.EntryJumpPlan, "SS Testy Boi II")
	jump, _ := entry.(*datastore.JumpPlan)

	jump.FromSystem = newSystem("nowhere")
	jump.ToSystem = newSystem("nohow")

	return jump
}
