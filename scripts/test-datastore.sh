#!/usr/bin/env bash

# Deploy the stack
export AWS_REGION="us-east-2"

aws cloudformation deploy --stack-name astropath-api-test --template-file aws/cloudformation/AstropathAPI.yaml \
  --capabilities CAPABILITY_NAMED_IAM

# Stupid cloudformation uses 255 as an exit code if the stack has no changes to deploy >.<
aws_exit=$?

if [ 0 -ne $aws_exit ] && [ 255 -ne $aws_exit ]; then
  echo "Got error code $aws_exit"
  exit $?
fi

set -e

# Get the table name
API_TABLE_NAME="$(aws cloudformation describe-stacks --stack-name astropath-api-test |
  grep -A 1 "ApiData" | grep "OutputValue" | cut -d '"' -f4)"

export API_TABLE_NAME

## Install the junit report generator
go get -u github.com/jstemmer/go-junit-report

# Set up the go environment
eval "$(go env)"

mkdir -p cover

TEST_TARGET="./datastore/..."

# Run the race tests
go test -race -short $TEST_TARGET

set +e

# Run the unit tests, including integration
go test --tags integration $TEST_TARGET 2>&1 | $GOPATH/bin/go-junit-report -set-exit-code >cover/test-report.xml

# We want to build the coverage reports regardless of test failure or success, so capture our
# test exit code for use later
exitCode=$?

if [ "0" == "${exitCode}" ]; then
  echo "PASS"
else
  echo "FAIL"
  exit $exitCode
fi
