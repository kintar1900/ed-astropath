#!/usr/bin/env bash

set -e

BRANCH=${CI_COMMIT_BRANCH:-$(git rev-parse --abbrev-ref HEAD)}
TAG=${CI_COMMIT_TAG:-$(git tag --points-at HEAD)}

# Versioning goes like this:
# 1. If we have a current tag, and that tag starts with 'v', use it
# 2. If we are on the 'develop' branch, use the git description to build a release candidate tag
# 3. Otherwise, just use the git description

DESCRIPTION=$(git describe --tags --dirty)

if [ -n "$TAG" ]; then
  VERSION=$(git describe --tags --dirty)
elif [ "$BRANCH" == "develop" ]; then
  VERSION=$(echo $DESCRIPTION | cut -d '-' -f 1)-rc.$(echo $DESCRIPTION | cut -d '-' -f 2-)
else
  VERSION=$(git describe --tags --dirty)
fi

echo "$VERSION"
