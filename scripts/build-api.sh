#!/usr/bin/env bash

set -e

TARGET=astropath/lambda
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

VERSION=$("$DIR"/parseVersion.sh)

LDFLAGS="-X main.version=${VERSION} -s -w"

set +e

upxBin="$(command -v upx)"

echo "Making directories...."

set -e

mkdir -p bin/api
cd bin/api

echo "Building API version ${VERSION}..."

GOOS="linux" GOARCH="amd64" go build -ldflags "${LDFLAGS}" $TARGET

if [ -n "$upxBin" ]; then
  upx -q lambda
fi

cd "$DIR"/..

rm -rf dist/ && mkdir dist/

zip dist/lambda.zip bin/api/lambda
