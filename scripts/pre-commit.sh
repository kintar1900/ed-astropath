#!/usr/bin/env bash

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
VERSION="$("$DIR"/parseVersion.sh | sed -e 's/-dirty//g')"
BRANCH=${CI_COMMIT_BRANCH:-$(git rev-parse --abbrev-ref HEAD)}

REPLACE="no"

if [ "master" == "$BRANCH" ]; then
  BADGE="!\\[Release Badge\\]\\(https:\\/\\/img.shields.io\\/static\\/v1\\?label=go.mod\\&message=${VERSION}\\&color=blue\\)"
  REPLACE="s/!\\[Release Badge\\].*/${BADGE}/g"
elif [ "develop" == "$BRANCH" ]; then
  BADGE="!\\[Dev Badge\\]\\(https:\\/\\/img.shields.io\\/static\\/v1\\?label=go.mod\\&message=${VERSION}\\&color=blue\\)"
  REPLACE="s/!\\[Dev Badge\\].*/${BADGE}/g"
fi

if [ "no" == "$REPLACE" ]; then
  echo "No updates to README"
else
  echo "Updating"
  sed -i -e "$REPLACE" "$DIR"/../README.md
fi
