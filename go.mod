module astropath

go 1.14

require (
	github.com/aws/aws-lambda-go v1.17.0
	github.com/aws/aws-sdk-go v1.33.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/kintar1900/elite-journalfile v1.2.0
	golang.org/x/net v0.0.0-20200625001655-4c5254603344 // indirect
)
