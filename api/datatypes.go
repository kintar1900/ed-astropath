// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package api

import "gitlab.com/kintar1900/elite-journalfile/event"

type PutData struct {
	AuthData
	EventMap map[string]interface{} `json:"Event"`
	Event    event.Event            `json:"-"`
}

type AuthData struct {
	CommanderName string `json:"Commander"`
	Token         string `json:"Token"`
}
