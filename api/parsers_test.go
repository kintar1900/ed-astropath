package api

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"log"
	"testing"
)

func TestParseAuthData(t *testing.T) {
	authDataJson := ` {
  "Commander": "Kintar",
  "Token": "ABCdefg",
  "Event": {
    "timestamp": "2020-01-01T00:00:00Z",
    "event": "Commander"
  }
} `
	authData := &AuthData{}
	err := json.Unmarshal([]byte(authDataJson), authData)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, "Kintar", authData.CommanderName)
	assert.Equal(t, "ABCdefg", authData.Token)
}
